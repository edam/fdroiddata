Categories:Science & Education
License:MIT
Web Site:
Source Code:https://github.com/crysxd/Studiportal-Checker
Issue Tracker:https://github.com/crysxd/Studiportal-Checker/issues

Auto Name:Studiportal
Summary:Check for test results at HS Furtwangen
Description:
Mit dieser App werdet ihr immer sofort informiert, wenn neue Noten
ins Studiportal eingetragen werden. Die App überprüft in einem
festen Interval, ob neue Noten verfügbar sind (natürlich alles
einstellbar). Ihr müsst nur eueren HFU-Benutzername und Passwort
eintragen.

Standardmäßig überprüft die App nur ob neue Noten vorhanden sind,
wenn ihr mit einem WLAN-Netzwerk verbunden seid, das könnt ihr aber
einfach umstellen wenn ihr wollt.
.

Repo Type:git
Repo:https://github.com/crysxd/Studiportal-Checker

Build:2.2.1,12
    commit=2.2.1
    subdir=Studiportal_Checker
    srclibs=JSoup@jsoup-1.7.3
    rm=Studiportal_Checker/libs/jsoup*.jar
    prebuild=pushd $$JSoup$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$JSoup$$/target/*jar libs/

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:2.2.1
Current Version Code:12

